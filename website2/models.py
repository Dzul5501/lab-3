from django.db import models
# Create your models here.
class Diary(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    nama = models.TextField(max_length=30)
    status = models.TextField(max_length=70)
