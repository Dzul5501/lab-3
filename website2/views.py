from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from datetime import datetime
from .models import Diary
# Create your views here.
diary_dict = {}
def index(request):
    diary_dict = Diary.objects.all().values()
    return render(request, "index2.html",{'diary_dict': diary_dict})
def add_activity(request):
    if request.method == 'POST':
        Diary.objects.create(nama =request.POST['nama'], status =request.POST['status'])
    return HttpResponseRedirect('/')